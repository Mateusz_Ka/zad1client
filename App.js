import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Picker } from 'react-native';
import { BACKEND_URL } from './config';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = { kategorie: [], czesci: [], selectedValue: undefined };
  }

  _changeCategory = val => fetch(`${BACKEND_URL}/czesci/${val}`)
    .then(res => res.json())
    .then(res => this.setState({ kategorie: this.state.kategorie, czesci: res, selectedValue: val }));

  componentDidMount() {
    fetch('${BACKEND_URL}/kategorie')
      .then(res => res.json())
      .then(res => this.setState({ kategorie: res }))
      .then(() => this._changeCategory(this.state.kategorie[0].id_kategorii))
      .catch(console.log)
  }

  render() {
    let picker = null;
    if (this.state.kategorie.length > 0) {
      picker = (
        <Picker onValueChange={this._changeCategory} selectedValue={this.state.selectedValue} style={styles.picker}>
          {
            this.state.kategorie.map(kat =>
              <Picker.Item label={kat.nazwa} value={kat.id_kategorii} key={kat.id_kategorii} />
            )
          }
        </Picker>
      )
    }


    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Magazyn</Text>
        {picker}
        <View style={styles.table}>
          {this.state.czesci.map(czesc => {
            return (
              <View key={czesc.id_czesci} style={styles.row}>
                <Text style={styles.item}>{czesc.id_czesci}</Text>
                <Text style={styles.item}>{czesc.id_kategorii}</Text>
                <Text style={styles.item2}>{czesc.nazwa}</Text>
                <Text style={styles.item3}>{czesc.opis}</Text>
              </View>
            )
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  table: {
    display: "flex",
    flexDirection: "column",
    marginRight: 10,
    marginLeft: 10,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    borderWidth: 5,
    borderBottomWidth: 2,
    borderColor: 'black',
    borderStyle: 'solid',
    height:500
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    borderBottomWidth: 3,
    borderBottomColor: 'black',
    borderStyle: 'solid',
  },
  picker: {
    borderColor: 'black',
    borderWidth: 10,
    borderStyle: 'solid'
  },
  item: {
    width: 30,
    padding: 5,
    borderRightColor: 'black',
    borderRightWidth: 1,
    borderStyle: 'solid'
  },
  item2: {
    width: 80,
    padding: 5,
    borderRightColor: 'black',
    borderRightWidth: 1,
    borderStyle: 'solid'
  },
  item3: {
    width: 245,
    padding: 5,
  },

});
